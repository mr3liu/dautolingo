# dAuTolingo

Google Chrome webdriver extension for automatic lesson completion on language-learning website Duolingo.

## 2020

Update: this project is currently defunct as Duolingo's website has undergone numerous changes.

## 2017

The whole script is made possible because Duolingo lessons now repeat questions.  
In one lesson, a maximum of 20 questions will appear, and the lesson goes on forever until all questions are answered.  
The script may require further modifying when Duolingo updates again.

(Remember to keep focus on page instead of console if console is open.)
