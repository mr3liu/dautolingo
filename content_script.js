////////////////////////////////////////////////////////////////////////////////////////////////////
/*

content_script.js
Copyright (c) 2017 by Liu Mingan Russell. All Rights Reserved.

Note: Potential bugs exist.

*/
////////////////////////////////////////////////////////////////////////////////////////////////////

(async function() {
	var STOP = false;
	
	var stop = function() {
		if (!STOP) {
			console.log("STOP -------------------------");
			STOP = true;
		}
	};

	//Stop by clicking icon
	chrome.runtime.onMessage.addListener(stop);
	
	//Stop by pressing Ctrl + C
	window.addEventListener("keydown", function(e) {
		if (e.ctrlKey && e.key == "c") {
			e.preventDefault(); //Prevent Ctrl + C copying
			stop();
		}
	}, false);
	
	var sleep = function(ms) { //Source: https://stackoverflow.com/a/39914235
		return new Promise(resolve => setTimeout(resolve, ms));
	};

	//Wait for page to load
	while (document.querySelector("._2wMRd")) { //Spinning "o" of Duolingo logo div
		if (STOP) {
			return;
		};
		console.log("Loading...");
		await sleep(100); 
	};
	
	//Check window location and lesson type, possibly change window location
	var pNm = window.location.pathname;
	if (pNm.slice(0, 7) == "/skill/") {
		if (pNm.slice(-9) == "/practice") { //Old lesson (Strengthen old skill)
			var newLsn = false;
		} else if (pNm.slice(-2, -1) == "/") { //New lesson (Start new skill)
			var newLsn = true;
		} else { //Skill page
			var a = document.querySelector("._2wH5q"); //New lesson "Start" a
			if (!a) {
				a = document.querySelector("._1_mnP"); //Old lesson "Strengthen" a
			};
			window.location = a.href;
			throw new Error("Navigating to the lesson page...");
		}
	} else if (pNm == "/") {
		if (document.getElementById("signup-link")) { //Login page
			document.getElementById("top_login").value = "[YOUR-USERNAME]";
			document.getElementById("top_password").value = "[YOUR-PASSWORD]";
			document.getElementById("login-button").click();
			throw new Error("Logging into [YOUR-USERNAME]...");
		} else { //Home page
			/*
			//Commented out code navigates to the newest skill in need of strengthening (if available), but sometimes strengthened skills do not increase in strength, resulting in and endless loop of strengthening the same skill.
			sk = document.querySelectorAll("._3_6C1:not(.dYBTa)"); //Unfull strength bar span
			if (sk[0]) {
				window.location = sk[sk.length - 1].parentNode.parentNode.href;
			} else {
				*/window.location = document.querySelector("._3qO9M ._2TMjc").parentNode.parentNode.href; //Incomplete lesson number of lessons completed out of total span
			/*};
			throw new Error("Navigating to the newest skill in need of strengthening...");*/
			throw new Error("Navigating to the oldest new skill...");
		}
	} else { //Other Duolingo page
		window.location = "https://www.duolingo.com";
		throw new Error("Navigating to the home page...");
	};
	
	console.log("START -------------------------");
	
	//Change background
	document.getElementsByClassName("_6t5Uh")[0].style.backgroundColor = "#000000";
	document.body.style.background = "linear-gradient(to bottom, #C70036 0%, #000000 100%)";

	var i = 0; //Counter
	var totQsnNo = 20; //Total number of questions
	var ansrs = [];	 //Answers
	var b1 = document.getElementsByTagName("button")[1]; //"Start practice/Check/Continue" button
	if (newLsn) {
		var twoStpVrf = false; //Two-step verification (of whether questions are repeating) is needed
	} else {
		b1.click(); //Click "Start practice" button
	};

	var chgElmVal = function(elm, ans) { //Change element value
		elm.focus(); //For responsive web design
		elm.value = ans;
		elm.blur();
	};

	//Main loop
	while (ansrs[0] || !i) {
		if (STOP || i == 60) { //i can only reach 60 when an error has occurred and the program keeps answering wrong answers
			return;
		};
		await sleep(500);
		var b2 = document.getElementsByTagName("button")[2]; //"Skip" button
		if (i < totQsnNo) {
			//Skip
			b2.click(); //Click "Skip" button
		} else {
			//Answer
			var ans = ansrs[0]; //Current answer
			var ta = document.getElementsByTagName("textarea")[0];
			if (i < totQsnNo * 2) {
				//Round 1
				if (ta) { //Textarea
					chgElmVal(ta, ans);
				} else {
					var ip = document.getElementsByTagName("input");
					if (ip[3]) { //Input(type="radio")[Der/Die/Das] and input(type="text") (with pictures)
						ans = ans.split(" "); //Separate "The" and noun
						chgElmVal(ip[3], ans[1]);
						for (var j=0; j<3; j++) {
							if (ip[j].value == ans[0]) {
								ip[j].click();
								break;
							}
						}
					} else if (ip[2]) {
						var dv = document.getElementsByClassName("_3EaeX"); //Select (NOT the element) options divs
						if (dv[0]) { //Input(type="checkbox")
							ans = ans.split(", ")
							if (ans[1]) {
								ans.push(ans.join(", ")) //Also answer unsplit answer in case it is one answer containing ", "(s). Potential bug: Multiple answers containing ", "(s)
							}
						} else { //Input(type="radio") (with pictures) (only in new lesson)
							var sp = document.querySelectorAll("._2dSPZ span:not(.eysNV)"); //Select (NOT the element) options spans
						};
						for (var j=0; j<3; j++) {
							if (dv[0]) {
								for (var k=0;  k<ans.length; k++) {
									if (dv[j].innerText == ans[k]) {
										dv[j].click();
										break;
									}
								}
							} else {
								if (sp[j].innerText == ans) {
									sp[j].click();
									break;
								}
							}
						}
					} else if (ip[0]) { //Input(type="text") (with/without pictures and audio)
						chgElmVal(ip[0], ans);
					} else { //Select
						var sl = document.getElementsByTagName("select")[0];
						for (var j=1; j<sl.options.length; j++) { //1, not 0, and - 1 two lines below, because "Select word" option is deleted when another option is selected
							if (sl.options[j].text == ans) {
								chgElmVal(sl, j - 1);
								break;
							}
						}
					}
				}
			} else {
				//Round 2: Occurs when answering the model answer in Round 1 is incorrect; the current only reason is that the model answer contains two acceptable answers: e.g. "He drinks., He is drinking." is the model answer to "Er trinkt." (German Basics-1). This only occurs on Textarea-type questions.
				ans = ans.split(", ");
				//Comments
				if (ans[5]) {
					ans = ans[0] + ", " + ans[1] + ", " + ans[2];//e.g. Answering "Please, hear him, my masters!" when "Please, hear him, my masters!, Please, hear her, my masters!" is the model answer to "Kostilus, ziry r?b?s, nuhis ?eks?s!" (High Valyrian Commands Lesson 3).
				} else if (ans[3]) {
					ans = ans[0] + ", " + ans[1];//e.g. Answering "No, thank you" when "No, thank you, No, thanks" is the model answer to "Non, merci" (French Common-Phrases).
				} else if (ans[2]) {
					ans = ans[1] + ", " + ans[2]; //e.g. Answering "Please, no." when "Please do not., Please, no." is the model answer to "Kostilus daor" (High Valyrian Phrases-1 Lesson 2). Potential bug: e.g. Model answer is "Please, no., Please do not."
				} else {
					ans = ans[0];
				};
				chgElmVal(ta, ans); //Answer only first answer. Potential bug: Answers containing an unusual number of ","s
			};
			b1.click(); //Click "Check" button
			ansrs.shift();
		};
		//Wait for answer to be graded
		while (!document.getElementsByClassName("_1cuVQ")[0]) { //"You are correct/Correct solution:" h2
			if (STOP) {
				return;
			};
			console.log("Grading...");
			await sleep(100); 
		};
		var mdlAns = document.getElementsByClassName("_34Ym5")[0]; //Model answer div
		var qsnNo = document.getElementsByClassName("_1jUjQ")[0].innerText; //Question number
		if (mdlAns) {
			//Record model answer
			mdlAns = mdlAns.innerText; //Model answer
			ansrs.push(mdlAns);
			console.log(qsnNo, "WRONG");
			console.log(mdlAns);
			if (newLsn) {
				if (mdlAns == ansrs[1] && twoStpVrf) { //Two-step verification: check if the second answer appears again, after the first answer appears again. Prevents repeating when a not-first answer is coincidentally the same as the first answer. Potential bug: Two consecutive not-first two answers that are coincidentally the same as the first two answers
					ansrs.splice(0, 2);
					i--; //Compensate for second repeated answer
					totQsnNo = i;
					i--; //Compensate for first repeated answer
					console.log("REPEAT -------------------------");
				} else if (mdlAns == ansrs[0] && i) { //Check if the first answer appears again, i.e. questions are repeating
					twoStpVrf = true;
				} else {
					twoStpVrf = false;
				}
			}
		} else {
			console.log(qsnNo, "RIGHT");
		};
		b1.click(); //Click "Continue" button
		i++;
	};
	
	console.log("END -------------------------");
	
	//Wait for lesson to finish
	await sleep(500);
	while (document.querySelector("._2wMRd")) { //Spinning "o" of Duolingo logo div
		if (STOP) {
			return;
		};
		console.log("Waiting...");
		await sleep(100); 
	};

	//Change window location
	window.location = "https://www.duolingo.com";
})(); //Invoke async function