////////////////////////////////////////////////////////////////////////////////////////////////////
/*

background.js
Copyright (c) 2017 by Liu Mingan Russell. All Rights Reserved.

*/
////////////////////////////////////////////////////////////////////////////////////////////////////

//Icon clicked
chrome.browserAction.onClicked.addListener(function() {
	chrome.tabs.executeScript({code: `
		if (window.location.hostname != "www.duolingo.com") {
			window.location = "https://www.duolingo.com";
			throw new Error("Navigating to Duolingo...");
		};
	`});
	//Send "STOP" message
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) { //Source: https://developer.chrome.com/extensions/messaging#simple
		chrome.tabs.sendMessage(tabs[0].id, {});
	});
});